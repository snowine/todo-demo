import React, {useEffect, useMemo, useRef} from 'react';
import type {Node} from 'react';
import type {TodoItem} from '../todos';
import {
  FlatList,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import DifficultyBar from './DifficultyBar';

const DAYS = [
  {label: 'Mo', weekday: true},
  {label: 'Tu', weekday: true},
  {label: 'We', weekday: true},
  {label: 'Th', weekday: true},
  {label: 'Fr', weekday: true},
  {label: 'Sa', weekday: false},
  {label: 'Su', weekday: false},
];

const CalendarItem: ({item: *}) => Node = ({item}) => {
  const window = useWindowDimensions();

  return (
    <View style={[styles.calendarItem, {width: window.width / 7}]}>
      <DifficultyBar
        difficulty={item.difficulty || 0}
        flexDirection={'column-reverse'}
      />
      <View style={{alignItems: 'center'}}>
        <Text style={{color: item.day.weekday ? 'black' : 'red'}}>
          {item.day.label}
        </Text>
      </View>
    </View>
  );
};

const Calendar: ({
  data: TodoItem[],
  firstViewableItemIndex: number,
}) => Node = ({data, firstViewableItemIndex}) => {
  const listRef = useRef();

  const calendarData = useMemo(() => {
    let i = 0;
    const result = [];
    while (i < data.length) {
      const currentDay = DAYS[result.length % DAYS.length];
      if (currentDay.weekday) {
        result.push({day: currentDay, itemIndex: i, ...data[i]});
        i++;
      } else {
        result.push({
          day: currentDay,
          id: `${currentDay.label}-${result.length}`,
        });
      }
    }
    return result;
  }, [data]);

  const firstCalendarItemIndex = useMemo(
    () => calendarData.findIndex(item => item.itemIndex === firstViewableItemIndex),
    [calendarData, firstViewableItemIndex],
  );

  useEffect(() => {
    listRef.current.scrollToIndex({index: firstCalendarItemIndex});
  }, [firstCalendarItemIndex]);

  return (
    <FlatList
      ref={listRef}
      style={styles.calendar}
      horizontal={true}
      data={calendarData}
      scrollEnabled={false}
      keyExtractor={item => item.id}
      renderItem={info => <CalendarItem item={info.item} />}
    />
  );
};

const styles = StyleSheet.create({
  calendar: {
    flex: 1,
  },
  calendarItem: {
    padding: 8,
  },
});

export default Calendar;
