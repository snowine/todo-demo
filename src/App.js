import React, {useState} from 'react';
import type {Node} from 'react';
import {LogBox, SafeAreaView, StyleSheet, View} from 'react-native';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {initialData} from '../todos';
import Calendar from './Calendar';
import TodoList from './TodoList';

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
]);

const App: () => Node = () => {
  const [data, setData] = useState(initialData);
  const [firstViewableItemIndex, setFirstViewableItemIndex] = useState(0);

  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.calendarView}>
          <Calendar
            data={data}
            firstViewableItemIndex={firstViewableItemIndex}
          />
        </View>
        <View style={styles.listView}>
          <TodoList
            data={data}
            setData={setData}
            firstViewableItemIndex={firstViewableItemIndex}
            onFirstViewableItemIndexChange={setFirstViewableItemIndex}
          />
        </View>
      </SafeAreaView>
    </GestureHandlerRootView>
  );
};

const styles = StyleSheet.create({
  calendarView: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 5,
  },
  listView: {
    flex: 2,
    backgroundColor: 'white',
  },
});

export default App;
