import React, {useCallback, useRef} from 'react';
import type {Node} from 'react';
import DraggableFlatList from 'react-native-draggable-flatlist/src/components/DraggableFlatList';
import {TodoItem} from '../todos';
import {RenderItemParams} from 'react-native-draggable-flatlist';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import DifficultyBar from './DifficultyBar';

const TodoList: ({
  data: TodoItem[],
  setData: (TodoItem[]) => void,
  firstViewableItemIndex: number,
  onFirstViewableItemIndexChange: number => void,
}) => Node = ({
  data,
  setData,
  firstViewableItemIndex,
  onFirstViewableItemIndexChange,
}) => {
  const listRef = useRef(null);
  const currentFirstVisibleIndex = useRef(firstViewableItemIndex);

  const renderItem = ({item, drag, isActive}: RenderItemParams<TodoItem>) => {
    return (
      <TouchableOpacity onLongPress={drag} disabled={isActive}>
        <View
          style={[
            styles.listItem,
            {backgroundColor: isActive ? 'gainsboro' : 'transparent'},
          ]}>
          <Text style={styles.todoText}>{item.task}</Text>
          <View style={styles.todoDifficulty}>
            <DifficultyBar
              difficulty={item.difficulty}
              flexDirection={'row-reverse'}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const handleViewableItemsChanged = useCallback(info => {
    currentFirstVisibleIndex.current = info.viewableItems[0].index;
  }, []);

  const handleScrollEnd = useCallback(() => {
    onFirstViewableItemIndexChange(currentFirstVisibleIndex.current);
    listRef.current.scrollToIndex({index: currentFirstVisibleIndex.current});
  }, [onFirstViewableItemIndexChange]);

  return (
    <DraggableFlatList
      data={data}
      ref={listRef}
      onDragEnd={({data}) => setData(data)}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      viewabilityConfig={{itemVisiblePercentThreshold: 50}}
      onViewableItemsChanged={handleViewableItemsChanged}
      onMomentumScrollEnd={handleScrollEnd}
    />
  );
};

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gainsboro',
    borderRadius: 7,
  },
  todoText: {
    color: 'black',
  },
  todoDifficulty: {
    width: '20%',
    height: '30%',
  },
});

export default TodoList;
