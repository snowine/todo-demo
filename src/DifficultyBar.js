import React, {useMemo} from 'react';
import type {Node} from 'react';
import {StyleSheet, View} from 'react-native';

const MAX_DIFFICULTY: number = 5;

// The tile dimension relative to the entire bar is calculated so the max difficulty takes up 95% of available space
const RELATIVE_TILE_DIMENSION: string = `${95 / MAX_DIFFICULTY}%`;

// The color lightness of each tile is decremented by growing difficulty. The decrement is calculated so the lightest
// tile has a lightness value of 80% and the darkest has 20%.
const LIGHTNESS_DECREMENT: number = 60 / (MAX_DIFFICULTY - 1);

const DifficultyBar: ({flexDirection: string, difficulty: number}) => Node = ({
  flexDirection,
  difficulty,
}) => {
  const {height: tileHeight, width: tileWidth} = useMemo(
    () =>
      flexDirection.includes('row')
        ? {height: '100%', width: RELATIVE_TILE_DIMENSION}
        : {height: RELATIVE_TILE_DIMENSION, width: '100%'},
    [flexDirection],
  );

  return (
    <View style={[styles.container, {flexDirection: flexDirection}]}>
      {Array(difficulty)
        .fill()
        .map((_, index) => (
          <View
            key={index}
            style={[
              styles.tile,
              {
                height: tileHeight,
                width: tileWidth,
                backgroundColor: `hsl(225,40%,${80 - index * LIGHTNESS_DECREMENT}%)`,
              },
            ]}
          />
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  tile: {
    marginVertical: 1,
    borderRadius: 7,
  },
});

export default DifficultyBar;
