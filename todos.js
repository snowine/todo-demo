export type TodoItem = {
  id: number,
  task: string,
  difficulty: number,
};

export const initialData: TodoItem[] = [
  {id: 1, task: 'Important task', difficulty: 2},
  {id: 2, task: 'Some other stuff to do', difficulty: 1},
  {id: 3, task: 'Another task', difficulty: 3},
  {id: 4, task: 'Complicated things', difficulty: 5},
  {id: 5, task: 'Favourite task', difficulty: 4},
  {id: 6, task: 'Task #666', difficulty: 2},
  {id: 7, task: 'This one is easy', difficulty: 1},
  {id: 8, task: 'Something to get rid of', difficulty: 3},
  {id: 9, task: 'Hard shit', difficulty: 5},
  {id: 10, task: 'More to do', difficulty: 4},
  {id: 11, task: 'This will be rather quick', difficulty: 2},
  {id: 12, task: 'Never forget', difficulty: 3},
  {id: 13, task: 'Another easy one', difficulty: 1},
  {id: 14, task: 'Very hard task', difficulty: 5},
  {id: 15, task: 'Task #1234', difficulty: 3},
  {id: 16, task: 'Favourite again', difficulty: 4},
];
